#include "GantChart.h"

#include <QtDebug>
#include <QPainter>
#include <algorithm>

void GantChart::paint(QPainter *painter) {
	QPen pen(QColor::fromRgb(0, 0, 0), 2);
	pen.setStyle(Qt::SolidLine);
	pen.setWidth(2);
	painter->setPen(pen);
	painter->fillRect(0, 0, width(), height(), QColor::fromRgb(255, 255, 255));
	qreal unitWidth, unitHeight, labelWidth;
	if (m_schedule == NULL || m_schedule->getSchedule().size() == 0) return;
	int maxEnd = 0;
	int gridLabelDisp = 1;
	const QVector<QVector<JssScheduleWrapper::Operation> > &sch = m_schedule->getSchedule();
	int machineCount = 1;
	for (int i = 0; i < sch.size(); ++i) {
		for (int j = 0; j < sch[i].size(); ++j) {
			machineCount = std::max(machineCount, sch[i][j].machine + 1);
			maxEnd = std::max(maxEnd, int(sch[i][j].endTime));
		}
	}
	painter->setRenderHints(QPainter::Antialiasing, true);

	labelWidth = (width() - 2 * m_margin) / sch.size() - m_legendHeight;
	for (int i = 0; i < sch.size(); ++i) {
		int x = m_margin + i * (labelWidth + m_legendHeight),
			y = m_margin, w = m_legendHeight, h = m_legendHeight;
		painter->fillRect(x, y, w, h, QColor::fromHsv(i * 360 / sch.size(), 255, 255));
		painter->drawRect(x, y, w, h);
		painter->drawText(x + m_legendHeight + m_spacing, y, labelWidth - m_spacing,
						  m_legendHeight, Qt::AlignLeft, QString::number(i + 1));
	}
	if (width() - 2 * m_margin <= 0 || height() - 3 * m_margin - m_legendHeight <= 0) return;
	gridLabelDisp = maxEnd / m_gridLabels;
	if (gridLabelDisp % 5) {
		gridLabelDisp = gridLabelDisp + 5 - (gridLabelDisp % 5);
	}
	//qDebug() << gridLabelDisp << " " << width();
	unitWidth = qreal(m_gridLabels * gridLabelDisp) / (width() - 2 * m_margin);
	//qDebug() << "UW:" << unitWidth;
	unitHeight = std::max((machineCount * (m_boxHeight + m_spacing) + m_spacing) /
						  (height() - 4 * m_margin  - 2 * m_legendHeight), 1.0);

	pen.setStyle(Qt::DashLine);
	pen.setWidth(1);
	painter->setPen(pen);
	for (int i = -1; i < m_gridLabels; ++i) {
		int x = m_margin + (i + 1) * gridLabelDisp / unitWidth,
			sY = 2 * m_margin + m_legendHeight,
			eY = height() - m_margin;
		painter->drawLine(x, sY + m_legendHeight + m_margin, x, eY);
		painter->drawText(x - gridLabelDisp / unitWidth, sY, 2 * gridLabelDisp / unitWidth, m_legendHeight, Qt::AlignHCenter,
						  QString::number((i + 1) * gridLabelDisp));
	}

	pen.setStyle(Qt::SolidLine);
	pen.setWidth(2);
	painter->setPen(pen);
	for (int i = 0; i < sch.size(); ++i) {
		for (int j = 0; j < sch[i].size(); ++j) {
			int sX = m_margin + sch[i][j].startTime / unitWidth,
				eX = m_margin + sch[i][j].endTime / unitWidth,
				sY = 2 * m_legendHeight + 3 * m_margin + sch[i][j].machine * (m_boxHeight + m_spacing) / unitHeight,
				eY = 2 * m_legendHeight + 3 * m_margin + ((sch[i][j].machine + 1) * (m_boxHeight + m_spacing) - m_spacing) / unitHeight;
			//qDebug() << "Rect:(" << sX << ", " << sY << "), (" << eX << ", " << eY << ")";
			painter->drawRect(sX, sY, eX - sX - 1, std::max(eY - sY - 1, 0));
			painter->fillRect(sX, sY, eX - sX - 1, std::max(eY - sY - 1, 0),
							  QColor::fromHsv(i * 360 / sch.size(), 255, 255));
		}
	}
}
