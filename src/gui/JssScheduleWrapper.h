#ifndef JSSSCHEDULEWRAPPER_H
#define JSSSCHEDULEWRAPPER_H

#include <QQuickItem>

#include <JssBase.h>
#include <vector>
#include <QVector>

class JssScheduleWrapper : public QObject {
	Q_OBJECT

public:
	struct Operation {
		Operation(jss::JssBase::Time startTime = 0, jss::JssBase::Time endTime = 0, jss::JssBase::Machine machine = -1) :
			startTime(startTime), endTime(endTime), machine(machine) {}
		jss::JssBase::Time startTime;
		jss::JssBase::Time endTime;
		jss::JssBase::Machine machine;
	};

	JssScheduleWrapper(const jss::JssBase::Schedule &schedule = jss::JssBase::Schedule(),
					   const std::vector<jss::JssBase::Job> &jobList = std::vector<jss::JssBase::Job>());
	const QVector<QVector<Operation> >& getSchedule() { return m_schedule; }
private:
	QVector<QVector<Operation> > m_schedule;
};

#endif // JSSSCHEDULEWRAPPER_H
