#include "JssEngine.h"

#include <QThreadPool>
#include <fstream>
#include <stdexcept>

JssEngine::~JssEngine() {
	if (tabuSearch != NULL) {
		delete tabuSearch;
	}
	if (genetic != NULL) {
		delete genetic;
	}
	if (hybrid != NULL) {
		delete hybrid;
	}
	if (tabuGen != NULL) {
		delete tabuGen;
	}
}

void JssEngine::start() {
	stopRequest = pauseRequest = false;
	m_status = 2;
	emit statusChanged();
	QThreadPool::globalInstance()->start(new JssRunable(this));
}

void JssEngine::stop() {
	if (m_status == 1) {
		if (tabuSearch != NULL) {
			delete tabuSearch;
			tabuSearch = NULL;
		}
		if (genetic != NULL) {
			delete genetic;
			genetic = NULL;
		}
		if (hybrid != NULL) {
			delete hybrid;
			hybrid = NULL;
		}
		m_status = 0;
		emit statusChanged();
	} else {
		stopRequest = true;
	}
}

void JssEngine::pause() {
	pauseRequest = true;
}

void JssEngine::resume() {
	stopRequest = pauseRequest = false;
	m_status = 2;
	emit statusChanged();
	QThreadPool::globalInstance()->start(new JssRunable(this));
}

void JssEngine::execute() {
	if (tabuSearch != NULL) {
		executeTabu();
	} else if (genetic != NULL) {
		executeGenetic();
	} else if (hybrid != NULL) {
		executeHybrid();
	} else if (tabuGen != NULL) {
		executeTabuGen();
	} else if (m_algorithm == "tabu") {
		executeTabu();
	} else if (m_algorithm == "genetic") {
		executeGenetic();
	} else {
		executeHybrid();
	}
}

void JssEngine::executeTabu() {
	if (tabuSearch == NULL) {
		std::ifstream fin(m_inputFile.toLocalFile().toStdString().c_str());
		jss::JssBase::InputType intype;
		if (!fin.is_open()) {
			m_status = 0;
			emit statusChanged();
			emit inputError();
			return;
		}
		if (m_inputType == "taillard") {
			intype = jss::JssBase::Taillard;
		} else if (m_inputType == "demirkol") {
			intype = jss::JssBase::Demirkol;
		} else {
			intype = jss::JssBase::Custom;
		}
		try {
			tabuSearch = new jss::JssTabuBack(fin, intype, lastTabuParams =
					jss::JssTabuParams(m_tabuSize, m_iterations,
									   m_backJumpSize, m_cycleSize, m_cycleRepetition));
		} catch (std::range_error) {
			delete tabuSearch;
			tabuSearch = NULL;
			m_status = 0;
			emit statusChanged();
			emit inputError();
			return;
		}

		m_schedule = new JssScheduleWrapper(tabuSearch->getBestSchedule(), tabuSearch->getJobList());
		m_bestMakespan = tabuSearch->getBestMakespan();
		m_totalIterations = 0;
		m_totalGenerations = 0;
		emit scheduleChanged();
		emit iterationCompleted();
		emit generationCompleted();
	}

	if (stopRequest) {
		m_status = 0;
		delete tabuSearch;
		tabuSearch = NULL;
		emit statusChanged();
		return;
	}
	if (pauseRequest) {
		m_status = 1;
		emit statusChanged();
		return;
	}
	//qDebug() << "Starting algorithm";
	while(tabuSearch->nextIteration()) {
		//qDebug() << "Iteration done";
		jss::JssBase::Time tmp = tabuSearch->getBestMakespan();
		if (tmp < m_bestMakespan) {
			//qDebug() << "New best makespan: " << tmp;
			m_bestMakespan = tmp;
			m_schedule = new JssScheduleWrapper(tabuSearch->getBestSchedule(), tabuSearch->getJobList());
			emit scheduleChanged();
			//qDebug() << m_status;
		}
		++m_totalIterations;
		emit iterationCompleted();
		if (stopRequest) {
			m_status = 0;
			delete tabuSearch;
			tabuSearch = NULL;
			emit statusChanged();
			return;
		}
		if (pauseRequest) {
			m_status = 1;
			emit statusChanged();
			return;
		}
	}
	saveToFile();
	delete tabuSearch;
	tabuSearch = NULL;

	m_status = 0;
	emit statusChanged();
}

void JssEngine::executeGenetic() {
	//qDebug() << "Executing genetig algorithm.";
	if (genetic == NULL) {
		std::ifstream fin(m_inputFile.toLocalFile().toStdString().c_str());
		jss::JssBase::InputType intype;
		if (!fin.is_open()) {
			m_status = 0;
			emit statusChanged();
			emit inputError();
			return;
		}
		if (m_inputType == "taillard") {
			intype = jss::JssBase::Taillard;
		} else if (m_inputType == "demirkol") {
			intype = jss::JssBase::Demirkol;
		} else {
			intype = jss::JssBase::Custom;
		}
		try {
			genetic = new jss::JssGen(fin, intype, lastGenParams =
					jss::JssGenParams(m_populationSize, m_eliteRate, m_extinctRate,
									  m_crossRate, m_generations));
		} catch (std::range_error) {
			delete genetic;
			genetic = NULL;
			m_status = 0;
			emit statusChanged();
			emit inputError();
			return;
		}
		//qDebug() << "Constructed\n";
		m_schedule = new JssScheduleWrapper(genetic->getBestSchedule(), genetic->getJobList());
		//qDebug() << "Schedule passed\n";
		m_bestMakespan = genetic->getBestMakespan();
		m_totalIterations = 0;
		m_totalGenerations = 1;
		emit scheduleChanged();
		emit iterationCompleted();
		emit generationCompleted();
	}

	if (stopRequest) {
		m_status = 0;
		delete genetic;
		genetic = NULL;
		emit statusChanged();
		return;
	}
	if (pauseRequest) {
		m_status = 1;
		emit statusChanged();
		return;
	}
	//qDebug() << "Starting algorithm";
	while(genetic->nextGeneration()) {
		//qDebug() << "Iteration done";
		jss::JssBase::Time tmp = genetic->getBestMakespan();
		if (tmp < m_bestMakespan) {
			//qDebug() << "New best makespan: " << tmp;
			m_bestMakespan = tmp;
			m_schedule = new JssScheduleWrapper(genetic->getBestSchedule(), genetic->getJobList());
			emit scheduleChanged();
			//qDebug() << m_status;
		}
		++m_totalGenerations;
		emit generationCompleted();
		if (stopRequest) {
			m_status = 0;
			delete genetic;
			genetic = NULL;
			emit statusChanged();
			return;
		}
		if (pauseRequest) {
			m_status = 1;
			emit statusChanged();
			return;
		}
	}

	saveToFile();
	delete genetic;
	genetic = NULL;

	m_status = 0;
	emit statusChanged();
}

void JssEngine::executeHybrid() {
	if (hybrid == NULL) {
		std::ifstream fin(m_inputFile.toLocalFile().toStdString().c_str());
		jss::JssBase::InputType intype;
		if (!fin.is_open()) {
			m_status = 0;
			emit inputError();
			emit statusChanged();
			return;
		}
		if (m_inputType == "taillard") {
			intype = jss::JssBase::Taillard;
		} else if (m_inputType == "demirkol") {
			intype = jss::JssBase::Demirkol;
		} else {
			intype = jss::JssBase::Custom;
		}
		try {
			hybrid = new jss::JssHybrid(fin, intype,
					lastGenParams = jss::JssGenParams(m_populationSize, m_eliteRate, m_extinctRate,
													  m_crossRate, m_generations),
					lastInternalTabuParams = jss::JssTabuParams(m_hTabuSize, m_hIterations, m_hBackJumpSize,
																m_hCycleSize, m_hCycleRepetition));
		} catch (std::range_error) {
			delete hybrid;
			hybrid = NULL;
			m_status = 0;
			emit statusChanged();
			emit inputError();
			return;
		}
		//qDebug() << "Constructed\n";
		m_schedule = new JssScheduleWrapper(hybrid->getBestSchedule(), hybrid->getJobList());
		//qDebug() << "Schedule passed\n";
		m_bestMakespan = hybrid->getBestMakespan();
		m_totalIterations = 0;
		m_totalGenerations = 1;
		emit scheduleChanged();
		emit iterationCompleted();
		emit generationCompleted();
	}

	if (stopRequest) {
		m_status = 0;
		delete hybrid;
		hybrid = NULL;
		emit statusChanged();
		return;
	}
	if (pauseRequest) {
		m_status = 1;
		emit statusChanged();
		return;
	}
	//qDebug() << "Starting algorithm";
	while(hybrid->nextGeneration()) {
		//qDebug() << "Iteration done";
		jss::JssBase::Time tmp = hybrid->getBestMakespan();
		if (tmp < m_bestMakespan) {
			//qDebug() << "New best makespan: " << tmp;
			m_bestMakespan = tmp;
			m_schedule = new JssScheduleWrapper(hybrid->getBestSchedule(), hybrid->getJobList());
			emit scheduleChanged();
			//qDebug() << m_status;
		}
		++m_totalGenerations;
		emit generationCompleted();
		if (stopRequest) {
			m_status = 0;
			delete hybrid;
			hybrid = NULL;
			emit statusChanged();
			return;
		}
		if (pauseRequest) {
			m_status = 1;
			emit statusChanged();
			return;
		}
	}

	tabuGen = new jss::JssTabuGen(hybrid->finishingTouch(
				lastTabuParams = jss::JssTabuParams(m_tabuSize, m_iterations, m_backJumpSize,
													m_cycleSize, m_cycleRepetition)));
	delete hybrid;
	hybrid = NULL;

	executeTabuGen();
}

void JssEngine::executeTabuGen() {
	if (stopRequest) {
		m_status = 0;
		delete tabuGen;
		tabuGen = NULL;
		emit statusChanged();
		return;
	}
	if (pauseRequest) {
		m_status = 1;
		emit statusChanged();
		return;
	}
	//qDebug() << "Starting algorithm";
	while(tabuGen->nextIteration()) {
		//qDebug() << "Iteration done";
		jss::JssBase::Time tmp = tabuGen->getBestMakespan();
		if (tmp < m_bestMakespan) {
			//qDebug() << "New best makespan: " << tmp;
			m_bestMakespan = tmp;
			m_schedule = new JssScheduleWrapper(tabuGen->getBestSchedule(), tabuGen->getJobList());
			emit scheduleChanged();
			//qDebug() << m_status;
		}
		++m_totalIterations;
		emit iterationCompleted();
		if (stopRequest) {
			m_status = 0;
			delete tabuGen;
			tabuGen = NULL;
			emit statusChanged();
			return;
		}
		if (pauseRequest) {
			m_status = 1;
			emit statusChanged();
			return;
		}
	}
	saveToFile();
	delete tabuGen;
	tabuGen = NULL;

	m_status = 0;
	emit statusChanged();
}

void JssEngine::saveToFile() {
	std::ofstream fout(m_outputFile.toLocalFile().toStdString().c_str());
	if (!fout.is_open()) return;
	if (tabuSearch != NULL) {
		fout << "ALGORITHM=Tabu-Search-1.0" << std::endl //promijeniti verziju kod buducih testiranja
			 << "INPUT_FILE=" << m_inputFile.toLocalFile().toStdString() << std::endl
			 << "INPUT_TYPE=" << m_inputType.toStdString() << std::endl
			 << "PARAMETERS={TL_SIZE=" << lastTabuParams.tabuMax
			 << ",ITERS="  << lastTabuParams.iterMax
			 << ",BJ_SIZE=" << lastTabuParams.maxL
			 << ",MAX_D=" << lastTabuParams.maxD << ",MAX_C=" << lastTabuParams.maxD << "}\n\n"
			 << "RESULTS:\n\n"
			 << "ITERATIONS=" << m_totalIterations << std::endl
			 << "MAKESPAN=" << tabuSearch->getBestMakespan() << std::endl
			 << "SCHEDULE=\n";

		const jss::JssBase::Schedule &schedule = tabuSearch->getBestSchedule();
		for (int i = 0; i < schedule.size(); ++i) {
			for (int j = 0; j < schedule[i].size(); ++j) {
				fout << schedule[i][j] << "\t";
			}
			fout << std::endl;
		}
	} else if (genetic != NULL) {
		fout << "ALGORITHM=Genetic-1.0" << std::endl //promijeniti verziju kod buducih testiranja
			 << "INPUT_FILE=" << m_inputFile.toLocalFile().toStdString() << std::endl
			 << "INPUT_TYPE=" << m_inputType.toStdString() << std::endl
			 << "PARAMETERS={POP_SIZE=" << lastGenParams.pop_size << ",PER_TOP=" << lastGenParams.per_top
			 << ",PER_BOTTOM=" << lastGenParams.per_bottom << ",P_CROSS=" << lastGenParams.p_cross
			 << ",MAX_G=" << lastGenParams.max_g << "}\n\n"
			 << "RESULTS:\n\n"
			 << "GENERATIONS=" << m_totalGenerations << std::endl
			 << "MAKESPAN=" << genetic->getBestMakespan() << std::endl
			 << "SCHEDULE=\n";

		const jss::JssBase::Schedule &schedule = genetic->getBestSchedule();
		for (int i = 0; i < schedule.size(); ++i) {
			for (int j = 0; j < schedule[i].size(); ++j) {
				fout << schedule[i][j] << "\t";
			}
			fout << std::endl;
		}
	} else if (tabuGen != NULL) {
		fout << "ALGORITHM=Hybrid-1.0" << std::endl //promijeniti verziju kod buducih testiranja
			 << "INPUT_FILE=" << m_inputFile.toLocalFile().toStdString() << std::endl
			 << "INPUT_TYPE=" << m_inputType.toStdString() << std::endl
			 << "PARAMETERS={POP_SIZE=" << lastGenParams.pop_size << ",PER_TOP=" << lastGenParams.per_top
			 << ",PER_BOTTOM=" << lastGenParams.per_bottom << ",P_CROSS=" << lastGenParams.p_cross
			 << ",MAX_G=" << lastGenParams.max_g << ",INNER={TL_SIZE=" << lastInternalTabuParams.tabuMax
			 << ",ITERS=" << lastInternalTabuParams.iterMax << ",BJ_SIZE=" << lastInternalTabuParams.maxL
			 << ",MAX_D=" << lastInternalTabuParams.maxD << ",MAX_C=" << lastInternalTabuParams.maxC
			 << "},FINISH={TL_SIZE=" << lastTabuParams.tabuMax << ",ITERS=" << lastTabuParams.iterMax
			 << ",BJ_SIZE=" << lastTabuParams.maxL << ",MAX_D=" << lastTabuParams.maxD
			 << ",MAX_C=" << lastTabuParams.maxC << "}}\n\n"
			 << "RESULTS:\n\n"
			 << "GENERATIONS=" << m_totalGenerations << std::endl
			 << "ITERATIONS=" << m_totalIterations << std::endl
			 << "MAKESPAN=" << tabuGen->getBestMakespan() << std::endl
			 << "SCHEDULE=\n";

		const jss::JssBase::Schedule &schedule = tabuGen->getBestSchedule();
		for (int i = 0; i < schedule.size(); ++i) {
			for (int j = 0; j < schedule[i].size(); ++j) {
				fout << schedule[i][j] << "\t";
			}
			fout << std::endl;
		}
	}
	fout.close();
}
