import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.0
import JssComponents 1.0

ApplicationWindow {
	property int margin: 11
	title: qsTr("Jss solver")
	width: mainLayout.implicitWidth + 2 * margin
	height: mainLayout.implicitHeight + 2 * margin
	visible: true

	JssEngine {
		id: jssEngine

		inputFile: Qt.resolvedUrl(inputFilename.text)
		inputType: taillardButton.checked ? "taillard" : (demirkolButton.checked ? "demirkol" : "custom")
		outputFile: Qt.resolvedUrl(outputFilename.text)

		algorithm: tabuSearchButton.checked ? "tabu" : (geneticButton.checked ? "genetic" : "hybrid")

		tabuSize: tabuListSize.text
		iterations: maxIterations.text
		backJumpSize: backJumpListSize.text
		cycleSize: maxDelta.text
		cycleRepetition: maxC.text

		generations: generations.text
		eliteRate: eliteRate.text
		extinctRate: extinctRate.text
		crossRate: crossRate.text
		populationSize: populationSize.text

		hTabuSize: hTabuListSize.text
		hIterations: hMaxIterations.text
		hBackJumpSize: hBackJumpListSize.text
		hCycleSize: hMaxDelta.text
		hCycleRepetition: hMaxC.text

		onInputError: inputErrorDialog.open()
	}

	FileDialog {
		id: inputFileDialog
		title: qsTr("Select input file")
		onAccepted: inputFilename.text = fileUrl.toString()
	}

	FileDialog {
		id: outputFileDialog
		title: qsTr("Select output file")
		onAccepted: outputFilename.text = fileUrl.toString()
		selectExisting: false
	}

	MessageDialog {
		id: inputErrorDialog
		title: "Input error"
		text: "Input file is corrupted."
		informativeText: "Is your selected input type correct?"
		icon: StandardIcon.Critical

	}

	ColumnLayout {
		id: mainLayout
		anchors.fill: parent
		anchors.margins: margin

		GroupBox {
			title: qsTr("File selection:")
			Layout.fillWidth: true
			ColumnLayout {
				anchors.fill: parent
				RowLayout {
					Layout.fillWidth: true
					Label { text: qsTr("Input file:") }
					TextField {
						id: inputFilename
						Layout.fillWidth: true
					}
					Button {
						text: qsTr("Browse...");
						onClicked: inputFileDialog.visible = true
					}
				}
				RowLayout {
					Layout.fillWidth: true
					Label { text: qsTr("Input type: ") }
					ExclusiveGroup { id: inputTypeGroup }
					RadioButton {
						id: taillardButton
						text: qsTr("Taillard")
						exclusiveGroup: inputTypeGroup
					}
					RadioButton {
						id: demirkolButton
						text: qsTr("Demirkol")
						exclusiveGroup: inputTypeGroup
					}
					RadioButton {
						id: customButton
						text: qsTr("Custom")
						exclusiveGroup: inputTypeGroup
						checked: true
					}
				}
				RowLayout {
					Layout.fillWidth: true
					Label { text: qsTr("Output file:") }
					TextField {
						id: outputFilename
						Layout.fillWidth: true
					}
					Button {
						text: qsTr("Browse...");
						onClicked: outputFileDialog.visible = true
					}
				}
			}
		}
		GroupBox {
			title: qsTr("Algorithm selection:")
			Layout.fillWidth: true
			ColumnLayout {
				anchors.fill: parent
				RowLayout {
					Layout.fillWidth: true
					ExclusiveGroup { id: algorithmSelectGroup }
					RadioButton {
						id: tabuSearchButton
						text: qsTr("tabu-search")
						exclusiveGroup: algorithmSelectGroup
						checked: true
					}
					RadioButton {
						id: geneticButton
						text: qsTr("genetic")
						exclusiveGroup: algorithmSelectGroup
					}
					RadioButton {
						id: hybridButton
						text: qsTr("hybrid")
						exclusiveGroup: algorithmSelectGroup
					}
				}

				RowLayout {
					id: rowLayout1
					Layout.fillWidth: true
					GroupBox {
						id: tabuGroup
						Layout.fillHeight: true
						Layout.fillWidth: true
						title: qsTr("Tabu-search options:")
						GridLayout {
							id: tabuControls
							anchors.right: parent.right
							anchors.rightMargin: 0
							anchors.left: parent.left
							anchors.leftMargin: 0
							anchors.top: parent.top
							flow: GridLayout.LeftToRight
							columns: 2
							Label { text: qsTr("Tabu list size:"); anchors.top: parent.top}
							IntInput {
								id: tabuListSize
								text: "10"
								Layout.fillWidth: true
								enabled: tabuSearchButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Iterations:") }
							IntInput {
								id: maxIterations
								text: "3500"
								Layout.fillWidth: true
								enabled: tabuSearchButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Back jump list size:") }
							IntInput {
								id: backJumpListSize
								text: "7"
								Layout.fillWidth: true
								enabled: tabuSearchButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Cycle size:") }
							IntInput {
								id: maxDelta
								text: "100"
								Layout.fillWidth: true
								enabled: tabuSearchButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Cycle repetition:") }
							IntInput {
								id: maxC
								text: "2"
								Layout.fillWidth: true
								enabled: tabuSearchButton.checked || hybridButton.checked
							}
						}
					}
					GroupBox {
						id: genGroup
						title: qsTr("Genetic options:")
						Layout.fillHeight: true
						Layout.fillWidth: true
						GridLayout {
							id: geneticControls
							anchors.right: parent.right
							anchors.rightMargin: 0
							anchors.left: parent.left
							anchors.leftMargin: 0
							anchors.top: parent.top
							flow: GridLayout.LeftToRight
							columns: 2
							Label {text: qsTr("Generations:") }
							IntInput {
								id: generations
								text: "400"
								Layout.fillWidth: true
								enabled: geneticButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Elites rate: ") }
							PercInput {
								id: eliteRate
								text: "0.1"
								Layout.fillWidth: true
								enabled: geneticButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Extinct rate: ") }
							PercInput {
								id: extinctRate
								text: "0.2"
								Layout.fillWidth: true
								enabled: geneticButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Cross rate: ") }
							PercInput {
								id: crossRate
								text: "0.7"
								Layout.fillWidth: true
								enabled: geneticButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Population size: ") }
							IntInput {
								id: populationSize
								text: "-1"
								Layout.fillWidth: true
								enabled: geneticButton.checked || hybridButton.checked
								validator: IntValidator { bottom: -1 }
							}
						}
					}
					GroupBox {
						id: hybridGroup
						title: qsTr("Hybrid internal search: ")
						Layout.fillHeight: true
						Layout.fillWidth: true
						GridLayout {
							id: hybridControls
							anchors.right: parent.right
							anchors.rightMargin: 0
							anchors.left: parent.left
							anchors.leftMargin: 0
							anchors.top: parent.top
							flow: GridLayout.LeftToRight
							columns: 2
							Label { text: qsTr("Tabu list size:"); anchors.top: parent.top}
							IntInput {
								id: hTabuListSize
								text: "10"
								Layout.fillWidth: true
								enabled: hybridButton.checked
							}
							Label { text: qsTr("Iterations:") }
							IntInput {
								id: hMaxIterations
								text: "2"
								Layout.fillWidth: true
								enabled: hybridButton.checked
							}
							Label { text: qsTr("Back jump list size:") }
							IntInput {
								id: hBackJumpListSize
								text: "0"
								Layout.fillWidth: true
								enabled: hybridButton.checked
							}
							Label { text: qsTr("Cycle size:") }
							IntInput {
								id: hMaxDelta
								text: "10"
								Layout.fillWidth: true
								enabled: hybridButton.checked
							}
							Label { text: qsTr("Cycle repetition:") }
							IntInput {
								id: hMaxC
								text: "2"
								Layout.fillWidth: true
								enabled: hybridButton.checked
							}
						}
					}
					GroupBox {
						id: resultsGroup
						title: qsTr("Results: ")
						Layout.fillHeight: true
						GridLayout {
							anchors.top: parent.top
							anchors.topMargin: 0
							anchors.right: parent.right
							anchors.rightMargin: 0
							anchors.left: parent.left
							anchors.leftMargin: 0
							flow: GridLayout.LeftToRight
							columns: 2
							Label { text: qsTr("Makespan: ") }
							TextField {
								text: jssEngine.makespan
								readOnly: true
								horizontalAlignment: Text.AlignRight
								width: 200
							}
							Label { text: qsTr("Iterations: ") }
							TextField {
								text: jssEngine.totalIterations
								readOnly: true
								horizontalAlignment: Text.AlignRight
								width: 200
								enabled: tabuSearchButton.checked || hybridButton.checked
							}
							Label { text: qsTr("Generations: ") }
							TextField {
								text: jssEngine.totalGenerations
								readOnly: true
								horizontalAlignment: Text.AlignRight
								width: 200
								enabled: geneticButton.checked || hybridButton.checked
							}
						}
					}
				}
			}
		}

		RowLayout {
			Layout.fillWidth: true
			Button {
				id: startButton
				text: jssEngine.status == 0 ? qsTr("Start") : qsTr("Stop")
				onClicked: {
					if (jssEngine.status == 0) {
						jssEngine.start();
					} else {
						jssEngine.stop();
					}
				}
			}
			Button {
				id: pauseButton
				text: jssEngine.status == 1 ? qsTr("Resume") : qsTr("Pause")
				enabled: jssEngine.status != 0
				onClicked: {
					if (jssEngine.status == 1) {
						jssEngine.resume()
					} else {
						jssEngine.pause()
					}
				}
			}
		}

		GantChart {
			id: gantChart
			Layout.fillHeight: true
			Layout.fillWidth: true
			implicitHeight: 120
			schedule: jssEngine.schedule
		}
	}

}
