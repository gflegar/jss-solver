#include <QApplication>
#include <QQmlApplicationEngine>

#include "GantChart.h"
#include "JssScheduleWrapper.h"
#include "JssEngine.h"

int main(int argc, char *argv[]) {
	QApplication app(argc, argv);

	qmlRegisterType<GantChart>("JssComponents", 1, 0, "GantChart");
	qmlRegisterType<JssScheduleWrapper>("JssComponents", 1, 0, "JssScheduleWrapper");
	qmlRegisterType<JssEngine>("JssComponents", 1, 0, "JssEngine");
	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

	return app.exec();
}
