import QtQuick 2.0
import QtQuick.Controls 1.2

TextField {
	horizontalAlignment: TextInput.AlignRight
	validator: IntValidator { bottom: 0 }
}
