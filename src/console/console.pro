QT -= gui
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

DESTDIR = ../../

OBJECTS_DIR = $$DESTDIR/obj/console
MOC_DIR = $$DESTDIR/moc/console
RCC_DIR = $$DESTDIR/rcc/console
UI_DIR = $$DESTDIR/ui/console

include(deployment.pri)
qtcAddDeployment()

LIBS += -L$$DESTDIR -ljss-tabu-lib
LIBS += -L$$DESTDIR -ljss-gen-lib

unix:!mac {
	QMAKE_LFLAGS += -Wl,-rpath=.
}

INCLUDEPATH += $$PWD/../jss-tabu-lib
INCLUDEPATH += $$PWD/../jss-gen-lib
DEPENDPATH += $$PWD/../jss-tabu-lib
DEPENDPATH += $$PWD/../jss-gen-lib

SOURCES += \
	main.cpp

unix:QMAKE_CXXFLAGS += -Wno-sign-compare -O3
