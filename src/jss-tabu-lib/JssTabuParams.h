#ifndef JSSTABUPARAMS
#define JSSTABUPARAMS

#include "JssTabuLibGlobal.h"
#include <cstdlib>
#include <iostream>
#include <cstring>

namespace jss {

class JSS_TABU_LIB_SHARED_EXPORT JssTabuParams {

public:
	/* const */ int tabuMax;
	/* const */ int iterMax;
	/* const */ int maxL; // back jump
	/* const */ int maxD;
	/* const */ int maxC;

	JssTabuParams(
		int _tabuMax, int _iterMax, int _maxL,
		int _maxD, int _maxC) :
			tabuMax(_tabuMax), iterMax(_iterMax), maxL(_maxL), maxD(_maxD), maxC(_maxC){}

	JssTabuParams(
		const char* _tabuMax = "20", const char* _iterMax = "1500", const char* _maxL = "10",
		const char* _maxD = "10", const char* _maxC = "2") :
			tabuMax(atoi(_tabuMax)), iterMax(atoi(_iterMax)), maxL(atoi(_maxL)),
			maxD(atoi(_maxD)), maxC(atoi(_maxC)){}
/*
	JssTabuParams(const char* type) {
		if(std::strcmp(type,"inner")==0) {
			JssTabuParams();
			std::cout << "inner\n";
		}
		else if(std::strcmp(type,"finish")==0) {
			JssTabuParams(20,1500,10,10,2);
			std::cout << "outer\n";
		}
		else{
			std::cout << "error";
		}

	}
*/
	JssTabuParams& operator=(const JssTabuParams& other ) {
		tabuMax = other.tabuMax;
		iterMax = other.iterMax;
		maxL    = other.maxL;
		maxD    = other.maxD;
		maxC    = other.maxC;
		return *this;
	}

};

}


#endif // JSSTABUPARAMS

