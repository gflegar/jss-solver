#include "JssTabu.h"
#include <algorithm>
#include <iostream>

namespace jss {


	JssBase::Move JssTabu::TabuList::insert(const Move &move) {
		Move ret(data[head]);
		std::multiset<Move>::iterator it = moves.find(ret);
		if (it != moves.end()) {
			moves.erase(it);
		}
		moves.insert(move);
		data[head++] = move;
		head %= data.size();
		return ret;
	}

	/**
	 *
	 */
	std::vector<JssBase::Move> JssTabu::getMoves(const std::vector<JssBase::Path>& blocks) {
		std::vector<Move> moves;

		int N = blocks.size();
		if (N <= 1) {
			return moves;
		}

		// Prvi blok.
		int tSize = blocks[0].size();
		if (tSize > 1) {
			moves.push_back(
				std::make_pair(blocks[0][tSize - 2], blocks[0][tSize - 1])); // Dodaj zadnji par.
		}

		// Srednji blokovi.
		for (int i = 1; i < N - 1; ++i) {
			tSize = blocks[i].size();
			if (tSize <= 1) {
				continue;
			}
			moves.push_back(std::make_pair(blocks[i][0], blocks[i][1])); // Dodaj prvi par.
			moves.push_back(
				std::make_pair(blocks[i][tSize - 2], blocks[i][tSize - 1])); // Dodaj zadnji par.
		}

		// Zadnji blok.
		tSize = blocks[N - 1].size();
		if (tSize > 1) {
			moves.push_back(std::make_pair(blocks[N - 1][0], blocks[N - 1][1])); // Dodaj prvi par.
		}

		return moves;
	}

	/**
	 *
	 */
	JssBase::OperationOrder JssTabu::generateOperationOrder(const JssBase::Move& move) const {
		OperationOrder genOrder(order);
		OperationIndex firstOp = move.first;
		OperationIndex secondOp = move.second;
		Machine m = getMachine(firstOp); // obje su na istoj masini

		// Nadji te operacije na stroju i zamijeni ih.
		for (int j = 0, endJ = genOrder[m].size(); j < endJ; ++j) {
			if (genOrder[m][j] == firstOp) {
				// TODO cini mi se da bi odmah iduca trebala bit secondOp
				genOrder[m][j] = secondOp;
				genOrder[m][j + 1] = firstOp;
				break;
			}
		}
		return genOrder;
	}

	/**
	 *
	 */
	void JssTabu::classifyMoves(const std::vector<JssBase::Move> &moves, std::vector<JssBase::Move> &permitedMoves,
								std::vector<JssBase::Time> &permitedMakespans,
								std::vector<JssBase::Move> &forbiddenProfitableMoves,
								std::vector<JssBase::Time> &forbiddenMakespans) {
		Time tmp;
		Schedule unused;
		permitedMoves.clear(); permitedMakespans.clear();
		forbiddenProfitableMoves.clear(); forbiddenMakespans.clear();

		for (int i = 0; i < moves.size(); ++i) {
			tmp = calculateMakespan(generateOperationOrder(moves[i]), unused);
			if (!tabuList.contains(moves[i])) {
				permitedMoves.push_back(moves[i]);
				permitedMakespans.push_back(tmp);
			} else if (tmp < makespan) {
				forbiddenProfitableMoves.push_back(moves[i]);
				forbiddenMakespans.push_back(tmp);
			}
		}
	}

}
