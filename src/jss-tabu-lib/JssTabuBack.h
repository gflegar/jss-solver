#ifndef _JSS_TABU_BACK_H_
#define _JSS_TABU_BACK_H_

#include "JssTabuLibGlobal.h"

#include "JssTabu.h"
#include "JssTabuParams.h"
#include <list>
#include <queue>



namespace jss {

class JSS_TABU_LIB_SHARED_EXPORT JssTabuBack : public JssTabu {

public:

	struct IterationContext {
		OperationOrder order;
		std::vector<Move> moves;
		TabuList tabuList;
		Time makespan;
		IterationContext(OperationOrder ord, std::vector<Move> mvs,
				TabuList tl, Time makespan)
			: order(ord), moves(mvs), tabuList(tl), makespan(makespan) {}
	};

	/**
	 * Klasa implementira detekciju ciklusa makespanova u algoritmu.
	 *
	 */
	class CycleDetector {
	private:
		int maxD; ///< Maksimalan period ciklusa
		int maxC; ///< vidi clanak
		int head; ///< indeks na koji ce biti ubacen prvi slijedeci element u `prevMakespans`
		std::vector<int> cycleSize; ///< Na i-tom mjestu je broj prethodnih ponavljanja za ciklus duljine `i`
		std::vector<Time> prevMakespans; ///< Popis zadnjih maxD vrijednosti makespana

	public:
		/**
		 * Konstruktor.
		 *
		 * @param maxD maksimalan period ciklusa
		 * @param maxC vidi clanak
		 */
		CycleDetector(int maxD, int maxC)
			: maxD(maxD), maxC(maxC), head(0), cycleSize(maxD), prevMakespans(maxD) {}
		/**
		 * Provjerava da li je zatvoren ciklus i pamti `makespan` za buduce pozive funkcije.
		 *
		 * Za sad je slozenost O(maxD). Treba promijeniti ako skuzimo O(1) slozenost.
		 */
		bool isCycle(Time makespan);
	};

protected:

	static const int defaultMaxL = 10;
	static const int defaultMaxD = 10;
	static const int defaultMaxC = 2;

	const int maxL; ///< maksimalna velicina L-a
	std::list<IterationContext> L;

	const int maxD;
	const int maxC;
	CycleDetector detector;

	bool save;
	int step;
	int iteration;
	OperationOrder bestOrder;
	Time bestMakespan;
	std::vector<Move> moves;

protected:
	/*JssTabuBack() : JssTabu(), maxL(defaultMaxL), maxD(defaultMaxD), maxC(defaultMaxC),
		detector(defaultMaxD, defaultMaxC), save(true), step(1), iteration(0) {}*/

	JssTabuBack(JssTabuParams params = JssTabuParams()) :
			JssTabu(params.tabuMax, params.iterMax),
			maxL(params.maxL), maxD(params.maxD), maxC(params.maxC),
			detector(params.maxD, params.maxC),
			save(true), step(1), iteration(0) {
	}


public:
	/**
	 * Konstruktor koji ucitava problem s input stream-a `is`. Format je odredjen s `inputType`.
	 *
	 * @param is Stream s kojeg se ucitava instanca problema
	 * @param inputType Tip inputa
	 */
    JssTabuBack(std::istream& is, InputType inputType, JssTabuParams params) :
            JssTabu(is, inputType, params.tabuMax, params.iterMax),
            maxL(params.maxL), maxD(params.maxD), maxC(params.maxC),
            detector(params.maxD, params.maxC),
            save(true), step(1), iteration(0) {
		generateStartingOrder();
		bestOrder = order;
		bestMakespan = makespan = calculateMakespan(order, schedule);
	}


	/*
	 * TSA with back jumping.
	 */
	void TSAB();

	/**
	 * Racuna slijedecu iteraciju algoritma.
	 *
	 * @return true ako postoji slijedeca iteracija, false ako je ovo bila zadnja.
	 */
	bool nextIteration();

	Time getBestMakespan() const {
		return bestMakespan;
	}

	Schedule getBestSchedule() const {
		Schedule bestSchedule;
		calculateMakespan(bestOrder, bestSchedule);
		return bestSchedule;
	}

    void setBestOrder(OperationOrder& oo) {
        bestOrder = oo;
    }

protected:

	/**
	 * Generira pocetni poredak.
	 */
	void generateStartingOrder();

	/**
	 * Vraca true ako je dosao do optimalnog rasporeda.
	 */
	bool neighborhoodSearchingStrategyBack();

};

}
#endif //_JSS_TABU_BACK_H_
