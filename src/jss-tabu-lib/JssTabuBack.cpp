#include "JssTabuBack.h"

#include<iostream>
#include<list>
#include<utility>
#include <algorithm>

namespace jss {

	bool JssTabuBack::CycleDetector::isCycle(JssTabuBack::Time makespan) {
		bool ret = false;
		for (int i = 0; i < maxD; ++i) {
			if (makespan == prevMakespans[(head + maxD - i) % maxD]) {
				++cycleSize[i];
			} else {
				cycleSize[i] = 0;
			}
			if (cycleSize[i] >= maxD * maxC) {
				ret = true;
			}
		}
		prevMakespans[head++] = makespan;
		head %= maxD;
		return ret;
	}

	/**
	*
	*/
	bool JssTabuBack::neighborhoodSearchingStrategyBack() {
		OperationOrder savedOrder(order);
		Time savedMakespan(makespan);
		TabuList savedTabuList(tabuList);

		if (moves.size() == 0) {
			return true;
		}

		Move bestMove;
		/*std::set<Move> forbiddenMoves = intersectWithTabuList(moves);
		std::set<Move> permittedMoves = differenceWithTabuList(moves);
		std::set<Move> forbiddenProfitableMoves = getForbiddenProfitableMoves(
			forbiddenMoves); */
		std::vector<Move> permittedMoves;
		std::vector<Move> forbiddenProfitableMoves;
		std::vector<Time> permittedMakespans;
		std::vector<Time> forbiddenMakespans;
		classifyMoves(moves, permittedMoves, permittedMakespans, forbiddenProfitableMoves, forbiddenMakespans);
		/* Step 1 */
		if (!forbiddenProfitableMoves.empty() || !permittedMoves.empty()) {
			Time C_min = -1;

			for (int i = 0; i < permittedMoves.size(); ++i) {
				if (C_min == -1 || permittedMakespans[i] < C_min) {
					C_min = permittedMakespans[i];
					bestMove = permittedMoves[i];
				}
			}
			for (int i = 0; i < forbiddenProfitableMoves.size(); ++i) {
				if (C_min == -1 || forbiddenMakespans[i] < C_min) {
					C_min = forbiddenMakespans[i];
					bestMove = forbiddenProfitableMoves[i];
				}
			}
			order = generateOperationOrder(bestMove);
			makespan = calculateMakespan(order, schedule);
		}
		/* Step 2 */
		else {
			if (moves.size() == 1) {
				bestMove = moves[0];
			} else {
				std::set<Move> tmp(moves.begin(), moves.end());
				while (true) {
					Move front = tabuList.insert(tabuList.back());
					if (tmp.count(front) > 0) {
						bestMove = front;
						break;
					}
				}
			}
			order = generateOperationOrder(bestMove);
			makespan = calculateMakespan(order, schedule);
		}

		// Jos razlike:
		if (save && moves.size() > 1) {
			moves.erase(std::find(moves.begin(), moves.end(), bestMove));
			L.push_back(IterationContext(savedOrder, moves, savedTabuList, savedMakespan));
			if (L.size() > maxL) {
				L.pop_front();
			}
		}
		save = false;
		/* Step 3 (otprilike) */
		tabuList.insert(std::make_pair(bestMove.second, bestMove.first));
		return false;
	}

	void JssTabuBack::TSAB() {
		/*bestOrder = order;
		bestMakespan = makespan = calculateMakespan(order, schedule);
		save = true;*/
		while(nextIteration());
		/*order = bestOrder;
		makespan = bestMakespan;*/
	}

	bool JssTabuBack::nextIteration() {
		if (step == 2) {
			IterationContext ctx = L.back();
			order = ctx.order;
			moves = ctx.moves;
			tabuList = ctx.tabuList;
			makespan = ctx.makespan;
			L.pop_back();
			iteration = 0;
			save = true;
		}
		++iteration;
		switch(step) {
		case 1:
			moves = getMoves(getMachineBlocks(getCriticalPath(schedule, order, 1)[0]));
		case 2:
			step = 1;
			if (neighborhoodSearchingStrategyBack()) {
				return false;
			}
		//case 3:
			if (makespan < bestMakespan) {
				bestOrder = order;
				bestMakespan = makespan;
				/*std::cout << "iteration: " << iteration << "     \n"
						  << "makespan : " << makespan << "\n\n";*/
				iteration = 0;
				save = true;
				return true;
			} /*else {
				std::cout << "iteration: " << iteration << "  \r";
				std::cout.flush();
			}*/
		//case 4:
			if (iteration < iterMax && !detector.isCycle(makespan)) {
				return true;
			}
		//case 5:
			if (L.size() == 0) {
				return false;
			}

			step = 2;
			return true;
		}
		return false;
	}

	struct JobData {
		JssBase::OperationIndex oi;
		JssBase::Time time;
		JobData(int job, int op, JssBase::Time time) : oi(job, op), time(time) {}
		friend bool operator<(const JobData &a, const JobData &b) {
			if (a.time == b.time) {
				return (a.oi.job == b.oi.job) ? a.oi.op < b.oi.op : a.oi.job < b.oi.job;
			}
			return a.time > b.time;
		}
	};

	/**
	 * Vraca duljinu najduljeg puta u rasporedu `order` koji prolazi kroz operaciju `oi`
	 * ili -1 ako raspored nije validan.
	 */
	JssBase::Time getLongestPath(const JssBase::OperationOrder &order, const std::vector<JssBase::Job> &jobList,
								 const JssBase::OperationIndex &oi) {
		std::vector<JssBase::Job> partialList(jobList.size());
		std::vector<std::vector<bool> > inPartialList(jobList.size());
		std::vector<std::vector<int> > partialListIndex(jobList.size());
		for (int i = 0; i < jobList.size(); ++i) {
			inPartialList[i].resize(jobList[i].size(), false);
			partialListIndex[i].resize(jobList[i].size(), -1);
		}
		//odredi koje operacije su u parcijalnom rasporedu
		for (int i = 0; i < order.size(); ++i) {
			for (int j = 0; j < order[i].size(); ++j) {
				inPartialList[order[i][j].job][order[i][j].op] = true;
			}
		}

		//std::cout << "OK\n";
		//generiraj parcijalni raspored i odredi nove indekse operacija
		for (int i = 0; i < jobList.size(); ++i) {
			for (int j = 0; j < jobList[i].size(); ++j) {
				if (inPartialList[i][j]) {
					partialListIndex[i][j] = partialList[i].size();
					partialList[i].push_back(jobList[i][j]);
				}
			}
		}

		//std::cout << "OK\n";
		//generiraj poredak po strojevima s novim indeksima
		JssBase::OperationOrder newOrder(order.size());
		for (int i = 0; i < order.size(); ++i) {
			for (int j = 0; j < order[i].size(); ++j) {
				JssBase::OperationIndex tmp = order[i][j];
				tmp.op = partialListIndex[tmp.job][tmp.op];
				newOrder[i].push_back(tmp);
			}
		}
		//std::cout << "OK\n";
		//algoritam slican onom za racunanje scheduela
		std::vector<std::vector<int> > done(partialList.size());
		std::vector<std::vector<JssBase::OperationIndex> > nextOperation(partialList.size());
		std::vector<std::vector<JssBase::Time> > longestPath[2];
		longestPath[0].resize(partialList.size());
		longestPath[1].resize(partialList.size());
		std::queue<JssBase::OperationIndex> frontier;
		JssBase::Time maxPath = 0;
		for (int i = 0; i < partialList.size(); ++i) {
			int tmp = partialList[i].size();
			done[i].resize(tmp, 0);
			nextOperation[i].resize(tmp, JssBase::OperationIndex(-1, -1));
			longestPath[0][i].resize(tmp, JssBase::Time(0));
			longestPath[1][i].resize(tmp, JssBase::Time(0));
			if (tmp > 0) {
				++done[i][0];
			}
		}
		for (int i = 0; i < newOrder.size(); ++i) {
			if (newOrder[i].size() > 0) {
				JssBase::OperationIndex tmp = newOrder[i][0];
				++done[tmp.job][tmp.op];
				if (done[tmp.job][tmp.op] == 2) {
					frontier.push(tmp);
				}
			}
			for (int j = 0; j < newOrder[i].size() - 1; ++j) {
				nextOperation[newOrder[i][j].job][newOrder[i][j].op] = newOrder[i][j + 1];
			}
		}

		while (!frontier.empty()) {
			JssBase::OperationIndex tmp = frontier.front();
			int job = tmp.job, op = tmp.op;
			JssBase::Time totalTime = longestPath[0][job][op] + partialList[job][op].time;
			JssBase::Time totalTimeFromOp = longestPath[1][job][0] + partialList[job][op].time;
			if (job == oi.job && op == oi.op) {
				longestPath[1][job][op] = longestPath[0][job][op];
			}
			maxPath = std::max(maxPath, longestPath[1][job][op]);
			frontier.pop();
			if (op + 1 < partialList[job].size()) {
				++done[job][op + 1];
				longestPath[0][job][op + 1] = std::max(longestPath[0][job][op + 1], totalTime);
				longestPath[1][job][op + 1] = std::max(longestPath[1][job][op + 1], totalTimeFromOp);
				if (done[job][op + 1] == 2) {
					frontier.push(JssBase::OperationIndex(job, op + 1));
				}
			}
			if (nextOperation[job][op].job != -1) {
				JssBase::OperationIndex next = nextOperation[job][op];
				++done[next.job][next.op];
				longestPath[0][next.job][next.op] = std::max(longestPath[0][next.job][next.op], totalTime);
				longestPath[1][next.job][next.op] = std::max(longestPath[1][next.job][next.op], totalTime);

				if (done[next.job][next.op] == 2) {
					frontier.push(next);
				}
			}
		}

		for (int i = 0; i < done.size(); ++i) {
			for (int j = 0; j< done[i].size(); ++j) {
				if (done[i][j] != 2) {
					return -1;
				}
			}
		}
		return maxPath;
	}

	/**
	 * Stavlja operaciju na najbolje mjesto.
	 */
	void addOperationToOrder(const JobData& job, JssBase::OperationOrder &order,
							 const std::vector<JssBase::Job> &jobList) {
		//std::cout << "=================== Adding operation (" << job.oi.job << ", " << job.oi.op <<") ==========\n";
		JssBase::Machine machine = jobList[job.oi.job][job.oi.op].machine;
		int bestPos = 0;
		JssBase::Time bestTime = -1;
		JssBase::Time currTime;
		order[machine].insert(order[machine].begin(), job.oi);
		bestTime = getLongestPath(order, jobList, job.oi);
		//printOrder(order);
		//std::cout << "Longest time: " << bestTime << std::endl;
		for (int i = 0; i < order[machine].size() - 1; ++i) {
			std::swap(order[machine][i], order[machine][i + 1]);
			if ((currTime = getLongestPath(order, jobList, job.oi)) != -1 && (bestTime == -1 || currTime < bestTime)) {
				bestPos = i + 1;
				bestTime = currTime;
			}
			//printOrder(order);
			//std::cout << "Longest time: " << currTime << std::endl;
		}
		std::rotate(order[machine].begin() + bestPos, order[machine].end() - 1, order[machine].end());
		//printOrder(order);
		//std::cout << "Longest time: " << bestTime << std::endl;
		//std::cout << "==========================================================\n";
	}


	void JssTabuBack::generateStartingOrder() {
		std::fill(order.begin(), order.end(), MachineOperationOrder());
		int longestJob = -1;
		Time jobLength = -1;
		// pronadji najdulji posao
		for (int i = 0; i < jobList.size(); ++i) {
			Time tmp = 0;
			for (int j = 0; j < jobList[i].size(); ++j) {
				tmp += jobList[i][j].time;
			}
			if (jobLength == -1 || tmp > jobLength) {
				jobLength = tmp;
				longestJob = i;
			}
		}
		//std::cout << "Najdulji posao : " << longestJob << std::endl;

		// dodaj najdulji posao u raspored
		for (int i = 0; i < jobList[longestJob].size(); ++i) {
			order[jobList[longestJob][i].machine].push_back(OperationIndex(longestJob, i));
		}
		//printOrder(order);
		// sortiraj operacije po trajanju! missssica
		std::vector<JobData> opList;
		for (int i = 0; i < jobList.size(); ++i) {
			if (i == longestJob) continue;
			for (int j = 0; j < jobList[i].size(); ++j) {
				opList.push_back(JobData(i, j, jobList[i][j].time));
			}
		}
		std::sort(opList.begin(), opList.end());
		// dodaj operacije u raspored
		for (int i = 0; i < opList.size(); ++i) {
			addOperationToOrder(opList[i], order, jobList);
			//printOrder(order);
		}
	}
}
