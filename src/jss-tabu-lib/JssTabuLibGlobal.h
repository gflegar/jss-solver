#ifndef _JSS_TABU_LIB_GLOBAL_H_
#define _JSS_TABU_LIB_GLOBAL_H_

#ifndef _NO_QT_
    #include <QtCore/qglobal.h>
    #ifdef JSS_TABU_LIB_LIBRARY
        #define JSS_TABU_LIB_SHARED_EXPORT Q_DECL_EXPORT
    #else
        #define JSS_TABU_LIB_SHARED_EXPORT Q_DECL_IMPORT
    #endif
#else
    #define JSS_TABU_LIB_SHARED_EXPORT
#endif

#endif // _JSS_TABU_LIB_GLOBAL_H_
