#include <queue>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <stdexcept>

#include "JssBase.h"

namespace jss {

	JssBase::JssBase(std::istream &is, InputType inputType) {
		int n, k, o;
		switch(inputType) {
		case Custom:
			//nas format inputa, kao u JssBase::JssBase(std::string)
			is >> n >> k;
			jobList.resize(n);
			order.resize(k);
			for (int i = 0; i < n; ++i) {
				is >> o;
				jobList[i].resize(o);
				for (int j = 0; j < o; ++j) {
					is >> jobList[i][j].machine >> jobList[i][j].time;
				}
			}
			break;
		case Taillard:
			is >> n >> k;
			jobList.resize(n);
			order.resize(k);
			for (int i = 0; i < n; ++i) {
				jobList[i].resize(k);
				for (int j = 0; j < k; ++j) {
					is >> jobList[i][j].time;
				}
			}
			for (int i = 0; i < n; ++i) {
				for (int j = 0; j < k; ++j) {
					is >> jobList[i][j].machine;
					--jobList[i][j].machine;
				}
			}
			break;
		case Demirkol:
			is >> n >> k;
			jobList.resize(n);
			order.resize(k);
			for (int i = 0; i < n; ++i) {
				jobList[i].resize(k);
				for (int j = 0; j < k; ++j) {
					is >> jobList[i][j].machine >> jobList[i][j].time;
				}
			}
			break;
		}
		sanityCheck();
	}

	/**
	 *
	 */
	JssBase::Time JssBase::calculateMakespan(const JssBase::OperationOrder &order,
		JssBase::Schedule &schedule) const {

			Time makespan = 0;
			std::vector<std::vector<OperationIndex> > nextOperation(jobList.size());
			std::vector<std::vector<char> > done(jobList.size());
			std::queue<OperationIndex> frontier;

			schedule.resize(jobList.size());
			/* Inicijalizira slijedece operacije na stroju, rasporeda i
			broja pristiglih bridova */
			for (int i = 0; i < jobList.size(); ++i) {
				nextOperation[i].resize(jobList[i].size());
				schedule[i].resize(jobList[i].size());
				done[i].resize(jobList[i].size());
				std::fill(nextOperation[i].begin(), nextOperation[i].end(), OperationIndex(-1, -1));
				std::fill(schedule[i].begin(), schedule[i].end(), 0);
				std::fill(done[i].begin(), done[i].end(), 0);
			}
			for (int i = 0; i < order.size(); ++i) {
				for (int j = 0; j < order[i].size() - 1; ++j) {
					nextOperation[order[i][j].job][order[i][j].op] = order[i][j + 1];
				}
				if (order[i].size()) {
					done[order[i][0].job][order[i][0].op] = 1;
				}
			}

			// stavi pocetne operacije u queue
			for (int i = 0; i < jobList.size(); ++i) {
				if (jobList[i].size()) {
					++done[i][0];
					if (done[i][0] == 2) {
						frontier.push(OperationIndex(i, 0));
					}
				}
			}

			// izracunaj raspored i makespan
			while (!frontier.empty()) {
				OperationIndex tmp = frontier.front();
				frontier.pop();
				int i = tmp.job, j = tmp.op;
				makespan = std::max(makespan, schedule[i][j] + jobList[i][j].time);

				if (j + 1 < jobList[i].size()) {
					// nisi zadnji u poslu
					schedule[i][j + 1] = std::max(schedule[i][j + 1],
						schedule[i][j] + jobList[i][j].time);
					if (++done[i][j + 1] == 2) {
						// svi uvjeti za pocetak slijedece operacije su zadovoljeni
						frontier.push(OperationIndex(i, j + 1));
					}
				}

				if (nextOperation[i][j].job != -1) {
					//nisi zadnji na stroju
					int ni = nextOperation[i][j].job, nj = nextOperation[i][j].op;
					schedule[ni][nj] = std::max(schedule[ni][nj],
						schedule[i][j] + jobList[i][j].time);
					if (++done[ni][nj] == 2) {
						frontier.push(OperationIndex(ni, nj));
					}
				}
			}
			return makespan;
	}


	/**
	 *
	 */
	std::vector<JssBase::Path> JssBase::getCriticalPath(
		const JssBase::Schedule &schedule, const JssBase::OperationOrder &order,
		int limit) const {
			std::vector<std::vector<OperationIndex> > prevOperation(schedule.size());
			Path pth;
			OperationIndex c, cjm[2];
			int tmp;
			Time makespan = 0;
			/* Pronadji kraj kriticnog puta */
			for (int i = 0; i < schedule.size(); ++i) {
				prevOperation[i].resize(schedule[i].size());
				if ((tmp = schedule[i].size()) && schedule[i][tmp - 1] + jobList[i][tmp - 1].time > makespan) {
					makespan = schedule[i][tmp - 1] + jobList[i][tmp - 1].time;
					c = OperationIndex(i, tmp - 1);
				}
			}
			pth.push_back(c);
			/* Zapisi prijasnju operaciju na stroju za svaku operaciju */
			for (int i = 0; i < order.size(); ++i) {
				for (int j = 0; j < order[i].size(); ++j) {
					int job = order[i][j].job, o = order[i][j].op;
					if (j == 0) {
						prevOperation[job][o] = OperationIndex(-1, -1);
					} else {
						prevOperation[job][o] = order[i][j - 1];
					}
				}
			}
			while (c.op > 0 || prevOperation[c.job][c.op].job != -1) {
				int pi = prevOperation[c.job][c.op].job, pj = prevOperation[c.job][c.op].op;
				if (pi != -1 && schedule[pi][pj] + jobList[pi][pj].time == schedule[c.job][c.op]) {
					// postoji prethodnik na istom stroju koji je na kriticnom putu
					cjm[0] = prevOperation[c.job][c.op];
				} else {
					cjm[0] = OperationIndex(-1, -1);
				}

				if (c.op > 0 && schedule[c.job][c.op - 1] + jobList[c.job][c.op - 1].time == schedule[c.job][c.op]) {
					// postoji prethodnik na istom poslu koji je na kriticnom putu
					cjm[1] = OperationIndex(c.job, c.op - 1);
				} else {
					cjm[1] = OperationIndex(-1, -1);
				}

				if (cjm[1].job != -1 && cjm[0].job != -1) {
					//std::cout << "Random!\n";
					c = cjm[rand() & 1];
				} else if (cjm[0].job != -1) {
					c = cjm[0];
				} else {
					c = cjm[1];
				}
				pth.push_back(c);
			}
			std::reverse(pth.begin(), pth.end());
			return std::vector<Path>(1, pth);
	}

	/**
	 *
	 */
	std::vector<JssBase::Path> JssBase::getMachineBlocks(Path& criticalPath) {
		std::vector<Path> blocks(1, Path(1, criticalPath[0]));
		for (int i = 1; i < criticalPath.size(); ++i) {
			if (getMachine(criticalPath[i - 1]) == getMachine(criticalPath[i])) {
				blocks[blocks.size() - 1].push_back(criticalPath[i]);
			} else {
				blocks.push_back(Path(1, criticalPath[i]));
			}
		}
		return blocks;
	}

	int JssBase::getMaxDuration() const {
		int max_dur = 0;
		for (int i = 0; i < jobList.size(); i++) {
			for (int j = 0; j < jobList[i].size(); j++) {
				if (jobList[i][j].time > max_dur) {
					max_dur = jobList[i][j].time;
				}
			}
		}
		return max_dur;
	}

	int JssBase::getNumberOfOperations() const {
	   // TODO je li mozemo ovo pretposaviti ?
	   // return jobList.size() * jobList[0].size();
	   int n_oper = 0;
	   for (int i = 0; i < jobList.size(); i++) {
		   n_oper += jobList[i].size();
	   }
	   return n_oper;
	}

	void JssBase::sanityCheck() const {
		int k = order.size();
		for (int i = 0; i < jobList.size(); ++i) {
			for (int j = 0; j < jobList[i].size(); ++j) {
				if (jobList[i][j].machine >= k) {
					throw std::range_error("Input corrupted");
				}
			}
		}
	}
}
