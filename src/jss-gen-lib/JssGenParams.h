#ifndef JSSGENPARAMS
#define JSSGENPARAMS

#include "JssGenLibGlobal.h"
#include <cstdlib>
#include <string>

namespace jss {

class JSS_GEN_LIB_SHARED_EXPORT JssGenParams {

public:
	/* const */ int pop_size;
	/* const */ double per_top;
	/* const */ double per_bottom;
	/* const */ double p_cross;
	/* const */ int max_g;

	JssGenParams(
		int _pop_size, double _per_top, double _per_bottom, double _p_cross, int _max_g) :
			pop_size(_pop_size), per_top(_per_top), per_bottom(_per_bottom), p_cross(_p_cross), max_g(_max_g){}

	JssGenParams(
		const char* _pop_size = "-1", const char* _per_top = "0.1", const char* _per_bottom = "0.2",
		const char* _p_cross = "0.7", const char* _max_g = "400") :
			pop_size(atoi(_pop_size)), per_top(atof(_per_top)), per_bottom(atof(_per_bottom)),
			p_cross(atof(_p_cross)), max_g(atoi(_max_g)){}

	JssGenParams& operator=(const JssGenParams& other ) {
		pop_size    = other.pop_size;
		per_top     = other.per_top;
		per_bottom  = other.per_bottom;
		p_cross     = other.p_cross;
		max_g       = other.max_g;
		return *this;
	}

};

}

#endif // JSSGENPARAMS

