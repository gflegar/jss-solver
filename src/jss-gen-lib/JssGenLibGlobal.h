#ifndef JSSGENLIB_GLOBAL_H
#define JSSGENLIB_GLOBAL_H

#ifndef _NO_QT_
	#include <QtCore/qglobal.h>
	#if defined(JSS_GEN_LIB_LIBRARY)
		#define JSS_GEN_LIB_SHARED_EXPORT Q_DECL_EXPORT
	#else
		#define JSS_GEN_LIB_SHARED_EXPORT Q_DECL_IMPORT
	#endif
#else
	#define JSS_GEN_LIB_SHARED_EXPORT
#endif

#endif // JSSGENLIB_GLOBAL_H
