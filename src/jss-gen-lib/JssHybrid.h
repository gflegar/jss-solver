#ifndef JSSHYBRID_H
#define JSSHYBRID_H

#include "JssGenLibGlobal.h"
#include "JssGen.h"

namespace jss {

class JSS_GEN_LIB_SHARED_EXPORT JssHybrid : public JssGen {
public:

	JssHybrid(std::istream &is, InputType inputType, JssGenParams genParams, JssTabuParams tabuParams)
		: JssGen(is, inputType, genParams, tabuParams) {}

	JssTabuGen decode(const Chromosome& c) const;
	int fitness(const Chromosome &c, Schedule &schedule) const;
	JssTabuGen finishingTouch(JssTabuParams finishParamss);

};

}
#endif // JSSHYBRID_H
