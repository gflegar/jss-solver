#include "JssHybrid.h"

#include <iostream>

namespace jss {

int JssHybrid::fitness(const Chromosome &c, Schedule &schedule) const {
	jss::JssTabuGen jss = decode(c);
	while(jss.nextIteration());
	schedule = jss.getBestSchedule();
	return jss.getBestMakespan();
}

JssTabuGen JssHybrid::decode(const Chromosome& c) const {
	JssTabuGen tabu_gen(c, jobList, getNumberOfMachines(), tabuParams);
	return tabu_gen;
}

JssTabuGen JssHybrid::finishingTouch(JssTabuParams finishParams) {
	jss::JssTabuGen jss(jobList, s_data[bestSchedule], finishParams);
    return jss;
    /*
	jss::JssBase::Time currentBest = jss.getBestMakespan();
	std::cout << "Starting finishing touch" << std::endl;
	int totalIters = 1;
	while(jss.nextIteration()) {
		jss::JssBase::Time tmp = jss.getBestMakespan();
		if (tmp < currentBest) {
			currentBest = tmp;
			std::cout << "Better solution found: " << currentBest << " (iteration: " << totalIters << ")\n";
			std::cout << "Iteration: " << totalIters << "\r";
			std::cout.flush();
		} else if ((totalIters - 1) % 100 == 0) {
			std::cout << "Iteration: " << totalIters << "\r";
			std::cout.flush();
		}
		++totalIters;
	}

	s_data[bestSchedule] = jss.getSchedule();
	best_fitness = jss.getBestMakespan();
    */
}


}
