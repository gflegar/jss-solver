JSS Solver
==========

JSS solver is an application that solves the Job Shop Scheduling problem.
More about the problem can be found on its
[wikipedia](https://en.wikipedia.org/wiki/Job_shop_scheduling) page.
Since the problem is NP-hard heuristic algorithms are implemented to solve it
approximately. More about the algorithms can be found in the _Algorithms_
section.

JSS solver provides a graphical user interface as well as console
commands for working with application. Aditionaly, all algorithms can be built
as shared libraries suitable for inclusion in other software.

Installation
------------

JSS Solver uses the powerfull Qt framework to implement its user interface.
Also, it uses `qmake` build system, so you will need both Qt libraries and
`qmake` installed to build JSS Solver.

After you clone (or download and unpacke the zip) this repository invoke the
following commands from the root folder of the repository:
```
$ qmake
$ make
```
(`$` represents your shell prompt)

This will create two executable files and shared libraries for specific
algorithms.
To use the GUI version call `gui` from the repositorys root folder, and to use
the console version call `console`.

Algorithms
----------

Currently, three diferent algorithms are implemented:

### Tabu search
This algorithm uses a tabu-search technique described in [1].
It searches the solution space localy, trying to improve the current solution
by changing the order of two consecutive jobs on the same machine.

### Genetic algorithm
The genetic algorithm described in [2] works with entire sets of solutions at
each iteration and tries to improve the solution by combining solution from
this set and mutating them.

### Hybrid algorithm
This is a combination of the first two algorithms. It finds the best results,
but it's also the slowest one. It uses the aproach from genetic algorithm but
improves each solution from the set by a short run of the first algorithm.

Screenshots
-----------

![Application GUI](https://bitbucket.org/gflegar/jss-solver/raw/master/screenshot.png)

References
----------

[1] Eugeniusz Nowicki, Czeslaw Smutnicki, _A fast taboo search algorithm for
the job shop problem_, Managment science/vol 42, No 6, pp. 797-813, June 1996.

[2] Jose Fernando Goncalves, Jorge Jose de Magalhaes Mendes, Maurıcio
G.C. Resende, _A hybrid genetic algorithm for the job shop scheduling problem_,
European Journal of Operational Research 167, pp. 77-95, 2005.

